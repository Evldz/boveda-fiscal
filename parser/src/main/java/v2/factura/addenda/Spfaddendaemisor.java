package v2.factura.addenda;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Spfaddendaemisor implements Serializable {

	
	private static final long serialVersionUID = 8682114212520517885L;
	@JsonProperty("DATOSFACTURAGLOBAL")
	Datosgspfacturaglobal datosgspfacturaglobal;	
	

}
