package v2.factura.addenda;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Datosgspfacturaglobal implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1458673018342706043L;
	@JsonProperty("TRANSACCIONES")
	Transacciones transacciones;
	

}
