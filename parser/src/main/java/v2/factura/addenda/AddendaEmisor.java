package v2.factura.addenda;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class AddendaEmisor implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1409475374725777298L;
	@JsonProperty("SPFADDENDAEMISOR")
	Spfaddendaemisor spfaddendaemisor;
	



}
