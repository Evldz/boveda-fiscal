package v2.factura;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;
import v2.factura.addenda.Adenda;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Item implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6149961225989523616L;
	
	@JsonProperty("Type")
	Integer Type;
	@JsonProperty("IsDirty")
	Boolean IsDirty;
	@JsonProperty("Id")
	String Id;
	@JsonProperty("IndexOrigin")
	String IndexOrigin;
	@JsonProperty("ReceivedDate")
	String ReceivedDate;
	@JsonProperty("ChangeDate")
	String ChangeDate;
	@JsonProperty("Uuid")
	String Uuid;
	@JsonProperty("Version")
	String Version;
	@JsonProperty("FechaEmision")
	String FechaEmision;
	@JsonProperty("FechaTimbrado")
	String FechaTimbrado;
	@JsonProperty("FormaDePago")
	String FormaDePago;
	@JsonProperty("MetodoDePago")
	String MetodoDePago;
	@JsonProperty("NoCertificado")
	String NoCertificado;
	@JsonProperty("TipoDeComprobante")
	String TipoDeComprobante;
	@JsonProperty("LugarExpedicion")
	String LugarExpedicion;
	@JsonProperty("Moneda")
	String Moneda;
	@JsonProperty("Serie")
	String Serie;
	@JsonProperty("Folio")
	String Folio;
	@JsonProperty("SubTotal")
	Double SubTotal;
	@JsonProperty("Total")
	Double Total; 
	@JsonProperty("TipoCambio")
	Double TipoCambio; 
	
	@JsonProperty("Receptor")
	Receptor receptor;
	
	
	@JsonProperty("MetaData")
	Metadata metadata;
	

	@JsonProperty("Adendas")
	List<Adenda> adendas;
	





}
