package v4.factura.addenda;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Pie implements Serializable{
	

	private static final long serialVersionUID = -4993902384820568671L;
	
	String id;
	
	@JsonProperty("Sucursal")
	String Sucursal;
	
	@JsonProperty("NumeroTicket")
	String NumeroTicket;
	
	@JsonProperty("IdTransaccion")
	String IdTransaccion;

	@Override
	public String toString() {
		return String.format("%s,%s,%s,%s\n", id, Sucursal, NumeroTicket,
				IdTransaccion);
	}
	
	

}
