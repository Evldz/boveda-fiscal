package v4.factura.addenda;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class AddendaEmisor implements Serializable {
	
	private static final long serialVersionUID = -1409475374725777298L;
	@JsonProperty("SPFAdicionalCFDI")
	SPFAdicionalCFDI spfAdicionalCfdi;
	
	public Boolean conInformacion() {

		if(spfAdicionalCfdi!=null && spfAdicionalCfdi.conInformacion()) return true;
		
		return false;
	}
	
	@Override
	public String toString() {

		return spfAdicionalCfdi!=null?spfAdicionalCfdi.toString():"";
	}
	
	
	public void cargaId(String id) {
		if(spfAdicionalCfdi!=null) spfAdicionalCfdi.cargaId(id);
	}
	
	


}
