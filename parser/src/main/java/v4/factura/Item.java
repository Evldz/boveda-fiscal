package v4.factura;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;
import v4.factura.addenda.Adenda;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Item implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6149961225989523616L;
	
	@JsonProperty("Id")
	String Id;
	@JsonProperty("Uuid")
	String Uuid;
	

	@JsonProperty("Adendas")
	List<Adenda> adendas;
	

	public Boolean conInformacion() {

		if(adendas.size() > 0) {
			for(Adenda a: adendas) {
				if(a.conInformacion()) return true;
			}
		}
		
		return false;
	}


	@Override
	public String toString() {
		String output="";
		for(Adenda a: adendas) {
			output += a.toString() ;
		}
		return output;
	}
	
	public void cargaId() {
		
		for(Adenda a: adendas) {
			a.cargaId(getId());
		}
		
	}





}

