package v4.factura;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Metadata implements Serializable{

	@JsonProperty("EMISON_idComprobante")
	String EMISON_idComprobante;
	
	@JsonProperty("EMISON_idEmisor")
	String EMISON_idEmisor;
	
	@JsonProperty("EMISON_idSucursal")
	String EMISON_idSucursal;
	
	@JsonProperty("EMISON_idTipoComprobante")
	String EMISON_idTipoComprobante;
	
	@JsonProperty("EMISON_numTransaccion")
	String EMISON_numTransaccion;
	
	@JsonProperty("FROM")
	String FROM;
	
	@JsonProperty("InternalReceivedDate")
	String InternalReceivedDate;
	
	@JsonProperty("InternalReceivedDateMx")
	String InternalReceivedDateMx;

}
