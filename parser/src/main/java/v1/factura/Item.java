package v1.factura;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Item implements Serializable {
	
	
	@JsonProperty("Type")
	Integer Type;
	@JsonProperty("IsDirty")
	Boolean IsDirty;
	@JsonProperty("Id")
	String Id;
	@JsonProperty("IndexOrigin")
	String IndexOrigin;
	@JsonProperty("ReceivedDate")
	String ReceivedDate;
	@JsonProperty("ChangeDate")
	String ChangeDate;
	@JsonProperty("Uuid")
	String Uuid;
	@JsonProperty("Version")
	String Version;
	@JsonProperty("FechaEmision")
	String FechaEmision;
	@JsonProperty("FechaTimbrado")
	String FechaTimbrado;
	@JsonProperty("FormaDePago")
	String FormaDePago;
	@JsonProperty("MetodoDePago")
	String MetodoDePago;
	@JsonProperty("NoCertificado")
	String NoCertificado;
	@JsonProperty("TipoDeComprobante")
	String TipoDeComprobante;
	@JsonProperty("LugarExpedicion")
	String LugarExpedicion;
	@JsonProperty("Moneda")
	String Moneda;
	@JsonProperty("Serie")
	String Serie;
	@JsonProperty("Folio")
	String Folio;
	@JsonProperty("SubTotal")
	Double SubTotal;
	@JsonProperty("Total")
	Double Total; 
	@JsonProperty("TipoCambio")
	Double TipoCambio; 
	
	@JsonProperty("Receptor")
	Receptor receptor;
	
	
	@JsonProperty("MetaData")
	Metadata metadata;
	
	
	
	private static final long serialVersionUID = 5313389657091129000L;
	








}
