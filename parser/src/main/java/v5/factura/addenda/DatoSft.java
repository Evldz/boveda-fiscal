package v5.factura.addenda;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class DatoSft {
	
	@JsonProperty("@FTSucursal")
	String FTSucursal;
	
	@JsonProperty("@FTIdTransaccion")
	String FTIdTransaccion;
	
	
	public Boolean conInformacion() {

		if(FTSucursal!=null || FTIdTransaccion!=null) return true;
		
		return false;
	}


	@Override
	public String toString() {
		return String.format("%s,%s", FTSucursal, FTIdTransaccion);
	}
	
	


}
