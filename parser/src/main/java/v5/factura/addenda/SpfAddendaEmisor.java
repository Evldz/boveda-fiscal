package v5.factura.addenda;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonPropertyOrder({"id","datoSft","datosOpcionales"})
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class SpfAddendaEmisor {
	
	@JsonProperty
	String id;
	
	@JsonProperty("DATOSFT")
	DatoSft datoSft;
	
	@JsonProperty("DATOSOPCIONALES")
	DatosOpcionales datosOpcionales;
	
	
	public Boolean conInformacion() {

		if((datoSft!=null && datoSft.conInformacion()) || 
			(datosOpcionales!=null && datosOpcionales.conInformacion())) return true;
		
		return false;
	}


	@Override
	public String toString() {
		return String.format("%s,%s,%s\n", id, datoSft, datosOpcionales);
	}
	
	

	
	
	

}
