package v5.factura.addenda;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class DatosOpcionales {

	@JsonProperty("@OPCampo70")
	String OpCampo70;
	
	public Boolean conInformacion() {

		if(OpCampo70!=null) return true;
		
		return false;
	}

	@Override
	public String toString() {
		return String.format("%s",  OpCampo70);
	}

}
