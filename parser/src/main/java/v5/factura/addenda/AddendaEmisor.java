package v5.factura.addenda;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class AddendaEmisor implements Serializable {
	

	private static final long serialVersionUID = 5153837559624254478L;
	@JsonProperty("SPFADDENDAEMISOR")
	SpfAddendaEmisor spfAddendaEmisor;
	
	public Boolean conInformacion() {

		if(spfAddendaEmisor!=null && spfAddendaEmisor.conInformacion()) return true;
		
		return false;
	}
	
	@Override
	public String toString() {

		return spfAddendaEmisor!=null?spfAddendaEmisor.toString():"";
	}
	
	
	public void cargaId(String id) {
		if(spfAddendaEmisor!=null) spfAddendaEmisor.setId(id);
	}
	

	
		


}
