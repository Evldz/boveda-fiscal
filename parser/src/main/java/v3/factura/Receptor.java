package v3.factura;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Receptor implements Serializable {
	
	@JsonProperty("Rfc")
	String Rfc;
	@JsonProperty("Nombre")
	String Nombre;
	@JsonProperty("UsoCFDI")
	String UsoCFDI;
	private static final long serialVersionUID = 7619359232371288755L;
	
}
