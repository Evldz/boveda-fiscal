package v3.factura.addenda;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Adenda implements Serializable {
	

	private static final long serialVersionUID = -741226221841332012L;
	@JsonProperty("AddendaEmisor")
	AddendaEmisor addendaEmisor;
	
	public Boolean conInformacion() {

		if(addendaEmisor!=null && addendaEmisor.conInformacion()) return true;
		
		return false;
	}
	
	@Override
	public String toString() {
	
		return addendaEmisor!=null?addendaEmisor.toString():"";
	}
	
	public void cargaId(String id) {
		if(addendaEmisor!=null) addendaEmisor.cargaId(id);
	}
	

	

}
