package v3.factura.addenda;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Spfaddendaemisor implements Serializable {

	
	private static final long serialVersionUID = 8682114212520517885L;
	@JsonProperty("DATOSFACTURAGLOBAL")
	Datosgspfacturaglobal datosgspfacturaglobal;	
	
	
	public Boolean conInformacion() {

		if(datosgspfacturaglobal!=null && datosgspfacturaglobal.conInformacion()) return true;
		
		return false;
	}
	
	@Override
	public String toString() {

		return datosgspfacturaglobal!=null?datosgspfacturaglobal.toString():"";
	}
	
	
	public void cargaId(String id) {
		if(datosgspfacturaglobal!=null) datosgspfacturaglobal.cargaId(id);
	}	

}
