package v3.factura.addenda;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Transacciones implements Serializable {	
	
	
	
	private static final long serialVersionUID = -2301569905278044220L;
	@JsonProperty("TRANSACCION")
	List<Transaccion> transaccion; 
	
	public Boolean conInformacion() {

		if(transaccion.size() >0) return true;
		
		return false;
	}
	
	@Override
	public String toString() {
		
		String output="";
		for(Transaccion t: transaccion) {
			output += t.toString();
		}

		return output;
	}
	
	
	public void cargaId(String id) {
		for(Transaccion t: transaccion) {
			t.setId(id);
		}
		
	}	



}
