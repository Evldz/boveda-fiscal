package v3.factura.addenda;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Transaccion implements Serializable{
	
	

	private static final long serialVersionUID = 1957238216244934262L;

	
	private String id;
	
	@JsonProperty("@IdTransaccion")
	private String IdTransaccion;
	
	@JsonProperty("@Total")
	private String Total;
	
	@JsonProperty("@FechaHora")
	private String FechaHora;

	@Override
	public String toString() {
		return String.format("%s,%s,%s,%s\n", id, IdTransaccion, Total,FechaHora);
	}
	
	
	

}
