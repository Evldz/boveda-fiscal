package v3.factura.addenda;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Datosgspfacturaglobal implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1458673018342706043L;
	@JsonProperty("TRANSACCIONES")
	Transacciones transacciones;
	
	
	public Boolean conInformacion() {

		if(transacciones!=null && transacciones.conInformacion()) return true;
		
		return false;
	}
	
	@Override
	public String toString() {

		return transacciones!=null?transacciones.toString():"";
	}
	
	
	public void cargaId(String id) {
		if(transacciones!=null) transacciones.cargaId(id);
	}	


}
