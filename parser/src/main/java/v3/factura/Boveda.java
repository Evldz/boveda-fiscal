package v3.factura;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class Boveda implements Serializable {
	

	@JsonProperty("Items")
	public  List<Item> Items ;
	private static final long serialVersionUID = -2526906537569856341L;

	
	/**
	 * Realiza la busqueda de Transaccion dentro de Transacciones
	 * @return
	 */

	public Boolean conInformacion() {
		
		if( Items.size() > 0) {
			for( Item i: Items) {
				if(i.conInformacion()) return true;
			}
		}
		return false;
	}
	

	@Override
	public String toString() {
		
		String output="";
		for(Item i: Items) {
			output +=i.toString();
		}
		return output;
	}
	
	
	public void cargaId() {
		for(Item i:Items) {
			i.cargaId();
		}
	}
	
	

}
